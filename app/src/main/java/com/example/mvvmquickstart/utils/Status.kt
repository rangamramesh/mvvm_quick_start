package com.example.mvvmquickstart.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}