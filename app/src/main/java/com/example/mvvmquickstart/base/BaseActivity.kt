package com.example.mvvmquickstart.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvmquickstart.R
import com.example.mvvmquickstart.utils.Constants

abstract class BaseActivity : AppCompatActivity() {

    val viewModelFactory by lazy {
        ViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)
    }

    @SuppressLint("Recycle", "CommitTransaction")
    open fun showProgressDialog() {
        try {
            val prev = supportFragmentManager.findFragmentByTag(Constants.PROGRESS_DIALOG)
            val ft = supportFragmentManager.beginTransaction()
            if (prev != null) ft.remove(prev)
            ft.addToBackStack(null)
            val newFragment = ProgressDialogFragment.newInstance()
            newFragment.isCancelable = true
            newFragment.setStyle(
                androidx.fragment.app.DialogFragment.STYLE_NO_TITLE,
                R.style.ProgressDialog
            )
            newFragment.show(supportFragmentManager, Constants.PROGRESS_DIALOG)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Dismiss the Progress Dialog
     */
    open fun dismissProgressDialog() {
        try {
            val prev = supportFragmentManager.findFragmentByTag(Constants.PROGRESS_DIALOG)
            if (prev != null) {
                val ft = supportFragmentManager.beginTransaction()
                ft.remove(prev)
                ft.commit()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}