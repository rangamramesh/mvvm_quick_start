package com.example.mvvmquickstart.login

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    @field:SerializedName("emailId")
    val email: String? = null,
    @field:SerializedName("password")
    val password: String? = null,
    @field:SerializedName("firstName")
    val firstName: String? = null,
    @field:SerializedName("lastName")
    val lastName: String? = null,
    @field:SerializedName("mobileNumber")
    val mobileNumber: String? = null
) : Parcelable