package com.example.mvvmquickstart.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvvmquickstart.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}