package com.example.mvvmquickstart

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.mvvmquickstart.base.BaseActivity
import com.example.mvvmquickstart.login.LoginViewModel
import com.example.mvvmquickstart.login.User
import com.example.mvvmquickstart.utils.Status
import com.example.mvvmquickstart.utils.displayNoActionAlertDialog
import com.example.mvvmquickstart.utils.toast

class MainActivity : BaseActivity() {
    private lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        getUses()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
    }

    private fun login(user: User) {
        viewModel.loginUser(user).observe(this, Observer {
            it?.let { response ->
                when (response.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        toast(Status.SUCCESS.name)

                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        displayNoActionAlertDialog(
                            "Error",
                            response.message ?: "Server Error, Please again later"
                        )
                    }
                }
            }
        })
    }

    private fun getUses() {
        viewModel.getUsers().observe(this, Observer {
            it?.let { response ->
                when (response.status) {
                    Status.LOADING -> {
                        showProgressDialog()
                    }
                    Status.SUCCESS -> {
                        dismissProgressDialog()
                        toast("${response.data?.size?: "0"} Posts")
                    }
                    Status.ERROR -> {
                        dismissProgressDialog()
                        displayNoActionAlertDialog(
                            "Error",
                            response.message ?: "Server Error, Please again later"
                        )
                    }
                }
            }
        })
    }
}