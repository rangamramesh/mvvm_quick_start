package com.example.mvvmquickstart.network.api

import com.example.mvvmquickstart.dummy.Posts
import com.example.mvvmquickstart.login.User
import retrofit2.http.Body
import retrofit2.http.GET

interface ApiService {

    @GET("posts")
    suspend fun getUsers(): List<Posts>

    @GET("login")
    suspend fun login(@Body user: User): User

}