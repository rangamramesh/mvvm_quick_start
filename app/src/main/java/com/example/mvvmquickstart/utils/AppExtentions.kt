package com.example.mvvmquickstart.utils

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mvvmquickstart.utils.Constants.INTENT_DATA


fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Context.displayNoActionAlertDialog(key: String, message: String) {
    AlertDialog.Builder(this).setTitle(key).setMessage(message)
        .setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }.create().show()
}

inline fun <reified T : AppCompatActivity> Context.startActivity(intentData: Any? = null) {
    val intent = Intent(this, T::class.java)
    intentData?.let { intent.putExtra(INTENT_DATA, intent) }
    startActivity(intent)
}