package com.example.mvvmquickstart.login

import com.example.mvvmquickstart.dummy.Posts
import com.example.mvvmquickstart.network.api.ApiService

class LoginRepository(private val apiService: ApiService) {

    suspend fun loginUser(user: User):User {
        return  apiService.login(user)
    }

   suspend fun getUsers(): List<Posts> {
       return apiService.getUsers()
    }

}