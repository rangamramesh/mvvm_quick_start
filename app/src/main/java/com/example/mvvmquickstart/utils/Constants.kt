package com.example.mvvmquickstart.utils

object Constants {
    const val PROGRESS_DIALOG = "progressDialog"
    const val SUCCESS = "Success"
    const val INTENT_DATA = "intent_data"

}
