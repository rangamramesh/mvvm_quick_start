package com.example.mvvmquickstart.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmquickstart.login.LoginRepository
import com.example.mvvmquickstart.login.LoginViewModel
import com.example.mvvmquickstart.network.api.RetrofitBuilder


class ViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(LoginRepository(RetrofitBuilder.apiService)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}

